/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javazadatak1;


import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.swap;
import java.util.List;

/**
 *
 * @author Mihael
 */
public class JavaZadatak1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
  
        ArrayList<String> stringList = new ArrayList<>();        
        
        stringList.add("baka");
        stringList.add("ana");
        stringList.add("zaba");
        stringList.add("eko");
        stringList.add("bicikl");
        stringList.add("romobil");
        stringList.add("Žaba");
        stringList.add("auto");
        stringList.add("zid");
        

        /* radi */
//        Provjera(stringList);
//         stringList.forEach((s) -> {
//            System.out.print(s +", ");
//        });
         
        /* radi */
//        BubbleStringListSort(stringList);        
//        stringList.forEach((s) -> {
//            System.out.print(s +", ");
//        });
        
        
        /* radi */
        BubbleSortDrugiNacin(stringList);        
        stringList.forEach((s) -> {
            System.out.print(s +", ");
        });
    }
    
    public static  void BubbleStringListSort(ArrayList<String> list){
        int duljinaListe = list.size();
        String privremeniSpremink;
        
        for (int vanjski = 0; vanjski< duljinaListe; vanjski++) {
            for(int unutarnji = vanjski+1 ; unutarnji < duljinaListe; unutarnji++){
                if(list.get(unutarnji).compareTo(list.get(vanjski))<0)
                {
                    privremeniSpremink = list.get(vanjski);
                    list.set(vanjski, list.get(unutarnji));
                    list.set(unutarnji, privremeniSpremink);
                }
            }
        }
    }
    
    
    public static  void BubbleSortDrugiNacin(ArrayList<String> list){
        int duljinaListe = list.size();
        
        for (int vanjski = 0; vanjski< duljinaListe; vanjski++) {
            for(int unutarnji = vanjski+1 ; unutarnji < duljinaListe; unutarnji++){
                if(list.get(unutarnji).compareTo(list.get(vanjski))<0)
                {
                    swap(list, unutarnji, vanjski);
                }
            }
        }      
    }
    
    
    
    public static void Provjera(List<String> list){
        Collections.sort(list);
    }
}
